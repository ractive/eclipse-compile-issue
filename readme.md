Eclipse compile issue with java 8
---------------------------------

Runs fine with "mvn test", but fails to compile in eclipse.

Change "maven.compiler.source" and "maven.compiler.target" to 1.7 in
pom.xml and it'll compile also in eclipse.