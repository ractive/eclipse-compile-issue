package com.example;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collection;
import java.util.Collections;

import org.junit.Test;

public class CompileIssueTest {

	private static class MyClass {
		@Override
		public boolean equals(Object obj) {
			return true;
		}

		@Override
		public int hashCode() {
			return 1;
		}
	}

	private static class Foo {
		public Collection <? extends MyClass> getIs() {
			return Collections.singleton(new MyClass());
		};
	}

	@Test
	public void test1() {
		Foo foo = new Foo();
		assertThat(foo.getIs()).containsOnly(new MyClass());
	}
}